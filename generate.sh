envsubst < ./java.hydra.yml > ./java.hydra.yml.proc.yml
envsubst < ./java.kratos.yml > ./java.kratos.yml.proc.yml
envsubst < ./java.keto.yml > ./java.keto.yml.proc.yml

# OPENAPI_GENERATOR_VERSION=4.3.1 openapi-generator-cli generate -i https://raw.githubusercontent.com/ory/sdk/master/spec/hydra/${HYDRA_VERSION}.json -g java -o ./hydra-client -c ./java.hydra.yml.proc.yml
# OPENAPI_GENERATOR_VERSION=4.3.1 openapi-generator-cli generate -i https://raw.githubusercontent.com/ory/sdk/master/spec/kratos/${KRATOS_VERSION}.json -g java -o ./kratos-client -c ./java.kratos.yml.proc.yml
# OPENAPI_GENERATOR_VERSION=4.3.1 openapi-generator-cli generate -i https://raw.githubusercontent.com/ory/sdk/master/spec/keto/${KETO_VERSION}.json -g java -o ./keto-client -c ./java.keto.yml.proc.yml

OPENAPI_GENERATOR_VERSION=4.3.1 openapi-generator-cli generate -i https://gitlab.com/JeffreyThijs/kratos-client-java/-/raw/master/api/openapi.yaml -g java -o ./kratos-client -c ./java.kratos.yml.proc.yml
